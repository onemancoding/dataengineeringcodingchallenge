#!/usr/bin/env python3
#encoding: UTF-8

import json
import string
import sys

FIELD_CHARS = string.ascii_lowercase
FIELD_SEPARATOR = ","

# Program entry point; parse command-line arguments
def main():
    if len(sys.argv) != 4:
        print("usage: {} <specfile> <datafile> <outputfile>".format(sys.argv[0]))
    else:
        specJson = load_spec(sys.argv[1])
        create_data(specJson, sys.argv[2])
        rows = load_data(specJson, sys.argv[2])
        parsed = parse_data(specJson, rows)
        write_csv(specJson, parsed, sys.argv[3])

# Load the JSON spec file
def load_spec(specFname):
    print("Loading spec file", specFname)
    with open(specFname) as f:
        specJson = json.load(f)
    print("Loaded spec file", specFname)
    return specJson

# Generate a file of fixed-with data
def create_data(specJson, dataFname, numRows=10):
    print("Writing data file", dataFname)
    with open(dataFname, "w", encoding=specJson['FixedWidthEncoding']) as f:
        for i in range(numRows):
            line = ""
            columnNum = 0
            for col in specJson["Offsets"]:
                columnLen = int(col)
                padChar = FIELD_CHARS[columnNum % len(FIELD_CHARS)]
                colText = padChar * columnLen
                line += colText
                columnNum += 1
            f.write(line)
            f.write("\n")
    print("Wrote data file", dataFname)

# Load the fixed-width data file into memory
def load_data(specJson, dataFname):
    print("Loading data file", dataFname)
    with open(dataFname, encoding=specJson['FixedWidthEncoding']) as f:
        lines = f.readlines()
    print("Loaded data file", dataFname)
    return lines

# Parse the fixed-width field data
def parse_data(specJson, rows):
    result = []
    for row in rows:
        fields = {}
        pos0 = 0
        columnNum = 0
        for offset in specJson['Offsets']:
            columnLen = int(offset)
            columnName = specJson['ColumnNames'][columnNum]
            pos1 = pos0 + columnLen
            columnVal = row[pos0:pos1].strip()
            fields[columnName] = columnVal
            pos0 = pos1
            columnNum += 1
        result.append(fields)
    return result

# Write the data to a CSV file
def write_csv(specJson, parsed, csvFname):
    with open(csvFname, "w", encoding=specJson['DelimitedEncoding']) as f:
        print("Writing CSV file", csvFname)
        if specJson["IncludeHeader"] == "True":
            f.write(FIELD_SEPARATOR.join(specJson["ColumnNames"]))
            f.write("\n")

        for row in parsed:
            f.write(FIELD_SEPARATOR.join(row[col] for col in specJson['ColumnNames']))
            f.write("\n")
    print("Wrote to CSV file", csvFname)

if __name__ == "__main__":
    main()
