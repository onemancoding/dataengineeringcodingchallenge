#!/usr/bin/env python3
#encoding: UTF-8

import unittest
from dataengineeringcodingchallenge.core import parse_data

spec = {
    "ColumnNames": ["f1", "f2", "f3"],
    "Offsets": ["5", "12", "3"],
    "FixedWidthEncoding": "windows-1252",
    "IncludeHeader": "True",
    "DelimitedEncoding": "utf-8"
}

rows = [
    'aaaaabbbbbbbbbbbbccc',
    'aaaaabbbbbbbbbbbbccc',
    'aaaaabbbbbbbbbbbbccc',
    'aaaaabbbbbbbbbbbbccc',
    'aaaaabbbbbbbbbbbbccc',
    'aaaaabbbbbbbbbbbbccc'
]

class TestParseData(unittest.TestCase):
    def test_parse_data(self):
        parsed = parse_data(spec, rows)
        self.assertEqual(6, len(parsed))
        row0 = parsed[0]
        self.assertEqual("aaaaa", row0["f1"])
        self.assertEqual("bbbbbbbbbbbb", row0["f2"])
        self.assertEqual("ccc", row0["f3"])

if __name__ == '__main__':
    unittest.main()
