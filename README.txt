Description
===========

Data Engineering Coding Challenge (DECC)

This code is designed for Python v3.

Build the executable with:
python3 setup.py install

After the executable is installed, run it with:
decc <specfile> <datafile> <outputfile>

...where <specfile> is the name of the JSON spec file; <datafile> is the fixed-field width input file; <outputfile> is the CSV output file
