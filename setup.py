from os.path import dirname
from os.path import join
from setuptools import find_packages
from setuptools import setup
import dataengineeringcodingchallenge

setup (
       name='DataEngineeringCodingChallenge',
       version=dataengineeringcodingchallenge.__version__,
       packages=find_packages(),

       # Declare your packages' dependencies here, for eg:
       #       install_requires=['foo>=3'],

       # Fill in these to make your Egg ready for upload to PyPI
       author='Greg Hawkes',
       author_email='greg.hawkes@global.ntt',

       #       url='',
       #       license='',
       long_description=open(join(dirname(__file__), 'README.txt')).read(),

       # could also include long_description, download_url, classifiers, etc.

       entry_points={'console_scripts':
       ['decc = dataengineeringcodingchallenge.core:main']
       }
       
       )